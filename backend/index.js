const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const expressFileUpload = require('express-fileupload');
const mongoose = require('mongoose');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(cors());
app.use(expressFileUpload());

app.use(express.static('public'));
mongoose.connect('mongodb+srv://moushree:m123456@cluster0.qbksq.mongodb.net/matrimony?retryWrites=true&w=majority', { useNewUrlParser: true },{ useUnifiedTopology: true });
const auth = require('./routing/auth');
const us = require('./routing/user');
app.use('/auth',auth);
app.use('/user',us);
app.listen(2000)
