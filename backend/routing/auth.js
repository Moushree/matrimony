const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();
const login = require('../model/login')
router.post('/register',async (req,res)=>{

    var img = req.files.profile;
    var imgname = Math.floor(Math.random()*100)+img.name;
    img.mv('./public/profileimg/'+imgname, async (err)=>{
        if(err)
        {
            throw err;
        }
        else
        {
            const  salt = await bcrypt.genSalt(10);
            const password = await bcrypt.hash(req.body.password,salt);
            var slug_check = await login.find({name:req.body.name});

            var slug_count = slug_check.length;
           
            var slug = req.body.name.toLowerCase();

        
            var slug = slug.replace(" ","-");

             if(slug_count>0){
            var slug = slug+slug_count;
             }
        
            var obj = {
                name:req.body.name,
                email:req.body.email,
                gandar:req.body.gendar,
                bod:req.body.bod,
                height:req.body.height,
                education:req.body.education,
                educationDetails:req.body.educationDetails,
                religion:req.body.religion,
                occupation:req.body.occupation,
                language:req.body.language,
                rashi:req.body.rashi,
                gotro:req.body.gotro,
                naksatra:req.body.naksatra,
                country:req.body.country,
                drinking:req.body.drinking,
                smocking:req.body.smocking,
                maretial:req.body.maretial,
                password:password,
                profile:imgname,
                slug:slug,
            } 
        
            await login.create(obj);
            res.send({msg:'Registered successfully'});
        }
    })
   

})
router.post('/login',async (req,res)=>{
    var result =  await login.find({email:req.body.email});
    if(result.length>0)
    {
        console.log(req.body.password);
        console.log(result[0].password);
  
        bcrypt.compare(req.body.password,result[0].password,(err,finalres)=>{
            if(err){
                throw err;
            }
            else
            {
                if(finalres==true)
                {
                    res.json(result[0])
                }
                else{
                    res.json({msg:"invalid user"})
                }
            }
        });
       
    }
    else
    {
      res.json({msg:"invalid user"});
    }
})

module.exports= router;