const mongoose = require('mongoose');
const loginSchema = new mongoose.Schema({
    name:String,
    email:String,
    gandar:String,
    height:Number,
    bod:String,
    education:String,
    educationDetails:String,
    religion:String,
    occupation:String,
    language:String,
    rashi:String,
    gotro:String,
    naksatra:String,
    country:String,
    drinking:String,
    smocking:String,
    maretial:String,
    password:String,
    profile:String,
    slug:String,
});

module.exports = mongoose.model("User",loginSchema);