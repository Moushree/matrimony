import {useState} from 'react';
function Register()
{

	let [name,setName]=useState('');
	let [email,setEmail]=useState('');
	let [gendar,setGendar]=useState('');
	let [bod,setBod]=useState('');
	let [height,setHight]=useState('');
	let [education,setEducation]=useState('');
	let [educationDetails,setEducationdetails]=useState('');
	let [religion,setReligion]=useState('');
	let [occupation,setOccupation]=useState('');
	let [language,setLanguage]=useState('');
	let [rashi,setRashi]=useState('');
	let [gotro,setGotro]=useState('');
	let [naksatra,setNaksatra]=useState('');
	let [country,setCountry]=useState('');
	let [drinking,setDrinking]=useState('');
	let [smocking,setSmocking]=useState('');
	let [maretial,setMaretial]=useState('');
	let [password,setPassword]=useState('');
	let [profile,setProfile]=useState(null);
	let [imgurl,setImgurl]=useState('');

    return(
        <div>

        
<div className="banner-w3ls inner-banner-agileits" id="home">
	<div className="container">
	
                 <div className="header-inner">
					  <h1 className="logo">
						<a href="index.html">Elite<span>Match</span></a></h1>
						<nav className="navbar navbar-default">
							<div className="navbar-header">
								<button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span className="sr-only">Toggle navigation</span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
								</button>
							</div>
						
							<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul className="nav navbar-nav navbar-right">
									<li><a href="index.html">Home</a></li>
									<li><a href="about.html">About</a></li>
									<li><a href="matches.html">Matches</a></li>
									<li className="dropdown menu__item">
										<a href="#" className="dropdown-toggle menu__link"  data-toggle="dropdown" data-hover="Pages" role="button" aria-haspopup="true" aria-expanded="false">Pages<span className="caret"></span></a>
										<ul className="dropdown-menu">
											<li><a href="codes.html">Short Codes</a></li>
											<li><a href="icons.html">Icons</a></li>
										</ul>
									</li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</div>
							<div className="clearfix"> </div>	
						</nav>
						<div className="header-right-w3ls">
							<a href="login.html">Log In</a>
							<a href="register.html">Register</a>
						</div>
						<div className="clearfix"></div>
				</div>
</div>			
</div>

<div className="w3l_agileits_breadcrumbs">
   <div className="container">
		<div className="w3l_agileits_breadcrumbs_inner">
			<ul>
				<li><a href="index.html">Home</a><span>&#187;</span></li>
									
				<li>Register</li>
				</ul>
		</div>
	</div>
</div>

<div className="login elite-app">
	<div className="container">
	<div className="tittle-agileinfo">
				<h3>Register Now</h3>
			</div>
	<div className="col-md-8 login-form-w3-agile">
			
		
				<div className="w3_form_body_grid">
					<span>Name</span>
					<input type="text" name="name" placeholder="Name" required="" onChange={(ev)=>{
						setName(ev.target.value)
					}}/>
				</div>
				<div className="w3_form_body_grid">
					<span>Email</span>
					<input type="email" name="Email" placeholder="Email" required="" onChange={(ev)=>{
						setEmail(ev.target.value)
					}}/>
				</div>
				<div className="w3_form_body_grid">
					<span>Gendar</span>
					<select id="w3_country1" className="frm-field required" name="gendar" onChange={(ev)=>{
						setGendar(ev.target.value)
					}}>
						<option>--Seletect Gender--</option>
						<option value="Male">Male</option>  
						<option value="Female">Female</option>   
						 						
					</select>
				</div>
				<div className="w3_form_body_grid w3_form_body_grid1">
					<span>Date Of Birth</span>
					<input className="date" id="datepicker" name="bod" type="date"   required="" onChange={(ev)=>{
						setBod(ev.target.value);
					}}/>
				</div>
				<div className="w3_form_body_grid">
					<span>Height(in cm)</span>
					<input type="text" name="height" placeholder="Height" required="" onChange={(ev)=>{
						setHight(ev.target.value)
					}}/>
				</div>
				<div className="w3_form_body_grid">
					<span>Education</span>
					<select className="frm-field required" name="education" onChange={(ev)=>{
						setEducation(ev.target.value)
					}}>
						<option value="null">--Select Education--</option> 
						<option value="Phd">Phd</option>  
						<option value="Post Graduate">Post Graduate</option>   
						<option value="Under Graduate">Under Graduate</option>   
						<option value="Higher Secondary">Higher Secondary</option>   
						<option value="Secondary">Secondary</option>   
						  						
					</select>
				</div>
				<div className="w3_form_body_grid">
					<span>Education Details</span>
					<input type="text" name="education-details" placeholder="Education Details" required="" onChange={(ev)=>{
						setEducationdetails(ev.target.value)
					}}/>
				</div>
				<div className="w3_form_body_grid">
					<span>religion</span>
					<select className="frm-field required" name="religion" onChange={(ev)=>{
						setReligion(ev.target.value)
}}>
						<option value="null">--Select Religion--</option> 
						<option value="Hindu">Hindu</option>  
						<option value="Muslim">Muslim</option>   
						<option value="Christian">Christian</option>   
						<option value="Sikh">Sikh</option>   
						<option value="Jain">Jain</option>   
						<option value="Buddhist">Buddhist</option>
						<option value="Anyone">No Religious Belief</option>   						
					</select>
				</div>
				<div className="w3_form_body_grid">
				<span>Occupation</span>
					<select className="frm-field required" name="occupation" onChange={(ev)=>{
						setOccupation(ev.target.value)
}}>
						<option value="null">--Select Occupation--</option> 
						<option value="Bussiness Owner">Bussiness Owner</option>
						<option value="Supervisor">Supervisor</option>
						<option value="Executive">Executive</option>
						<option value="Manager">Manager</option>
						<option value="Officer">Officer</option>
						<option value="Engineer">Engineer</option>   
						<option value="Doctor">Doctor</option>   
						<option value="Teacher">Teacher</option>   
						<option value="Other">Other</option>   						
					</select>
				</div>
				<div className="w3_form_body_grid">
				<span>Language</span>
					<select className="frm-field required" name="language" onChange={(ev)=>{
						setLanguage(ev.target.value)
}}>
						<option value="null">--Select Language--</option> 
						<option value="English">English</option>
						<option value="Hindi">Hindi</option>
						<option value="Bangla">Bangla</option>
						<option value="Gujarati"> Gujarati</option>
						<option value="Tamil">Tamil</option>
						<option value="Telugu">Telugu</option>   
						<option value="Sindhi">Sindhi</option>   
						<option value="Malayalam">Malayalam</option>   
						<option value="Other">Other</option>   						
					</select>
				</div>
				<div className="w3_form_body_grid">
				<span>Rashi:</span>
					<select className="frm-field required" name="rashi" onChange={(ev)=>{
						setRashi(ev.target.value)
}}>
						<option value="null">--Select Language--</option> 
						<option value="Aries">Aries </option>
						<option value="Taurus">Taurus</option>
						<option value="Gemini">Gemini </option>
						<option value="Cancer"> Cancer </option>
						<option value="Leo">Leo </option>
						<option value="Virgo">Virgo </option>   
						<option value="Libra">Libra   </option>   
						<option value="Scorpio">Scorpio </option>   
						<option value="Sagittarius">Sagittarius </option> 
						<option value="Capricorn">Capricorn  </option> 
						<option value="Aquarius">Aquarius  </option>   	
						<option value="Pisces">Pisces   </option>   					
					</select>
				</div>
				<div className="w3_form_body_grid">
				<span>Gotro</span>
					<select name="gotro" className="frm-field required" onChange={(ev)=>{
						setGotro(ev.target.value)
}}>
						<option value="null">--Select Gotro--</option> 
						<option value="Shandilya">Shandilya</option>
						<option value="Gautama Maharishi">Gautama Maharishi</option>
						<option value="Bharadwaja">Bharadwaja</option>
						<option value="Vishvamitra"> Vishvamitra</option>
						<option value="Jamadagni">Jamadagni</option>
						<option value="Vashista">Vashista</option>   
						<option value="Kashyapa">Kashyapa </option>   
						<option value="Atri">Atri</option>   
						<option value="Other">Other</option>   						
					</select>
				</div>
				<div className="w3_form_body_grid">
				<span>Naksatra:</span>
					<select className="frm-field required" name="naksatra" onChange={(ev)=>{
						setNaksatra(ev.target.value)
}}>
						<option value="null">--Select Naksatra--</option> 
						<option value="Ashvini">Ashvini</option>
						<option value="Bharani">Bharani</option>
						<option value="Krittika">Krittika</option>
						<option value="Rohini"> Rohini</option>
						<option value="Mrigashīrsha">Mrigashīrsha</option>
						<option value="Ardra">Ardra</option>   
						<option value="Punarvasu">Punarvasu  </option>   
						<option value="Pushya">Pushya</option>   
						<option value="Other">Other</option>   						
					</select>
				</div>
				
				<div className="w3_form_body_grid w3_form_body_grid1">
					<span>Country</span>
					<input type="text" name="Country" placeholder="Country" required="" onChange={(ev)=>{
						setCountry(ev.target.value)
}}/>
				</div>
				<div className="w3_form_body_grid">
				<span>Drinking</span>
					<select name="drinking" className="frm-field required" onChange={(ev)=>{
						setDrinking(ev.target.value)
}}>
						<option value="null">--Select--</option> 
						<option value="Yes">Yes</option>
						<option value="No">No</option>   
						<option value="Occationaly">Occationaly</option>   						
					</select>
				</div>
				<div className="w3_form_body_grid">
				<span>Smocking:</span>
					<select name="smocking" className="frm-field required" onChange={(ev)=>{
						setSmocking(ev.target.value)
}}>
						<option value="null">--Select--</option> 
						<option value="Yes">Yes</option>
						<option value="No">No</option>   
						<option value="Occationaly">Occationaly</option>   						
					</select>
				</div>
				<div className="w3_form_body_grid">
				<span>Maretial Status:</span>
					<select name="maretial" className="frm-field required" onChange={(ev)=>{
						setMaretial(ev.target.value)
}}>
						<option value="null">--Select--</option> 
						<option value="Single">Single</option>
						<option value="Widow">Widow</option>   
						<option value="Divorced">Divorced</option>   						
					</select>
				</div>
				<div className="w3_form_body_grid w3_form_body_grid1">
					<span>Password</span>
					<input type="password" name="Password" placeholder="Password" required="" onChange={(ev)=>{
						setPassword(ev.target.value)
					}}/>
				</div>
				<div className="w3_form_body_grid w3_form_body_grid1">
					<span>Profile Picture</span>
					<label><input type="file" name="pimg" placeholder="Profile" required="" className="profile-btn" onChange={(ev)=>{
						setProfile(ev.target.files[0])

						setImgurl(URL.createObjectURL( ev.target.files[0] ));
						
					}}/><span className="btn btn-primary sbtn">Upload</span></label>
					<p>
					{imgurl?<img src={imgurl} className="profile-img"/>:''}
					</p>
				</div>
				
				<div></div>
				<input type="button" value="Sign Up" onClick={ async (ev)=>{

                 if(name == ""){
                          alert("Please Enter Your Name")
				 }else{

				 





					var fd = new FormData()
					fd.append('name',name);
					fd.append('email',email);
					fd.append('gendar',gendar);
					fd.append('bod',bod);
					fd.append('height',height);
					fd.append('education',education);
					fd.append('educationDetails',educationDetails);
					fd.append('religion',religion);
					fd.append('occupation',occupation);
					fd.append('language',language);
					fd.append('rashi',rashi);
					fd.append('gotro',gotro);
					fd.append('naksatra',naksatra);
					fd.append('country',country);
					fd.append('drinking',drinking);
					fd.append('smocking',smocking);
					fd.append('maretial',maretial);
					fd.append('password',password);
					fd.append('profile',profile);

					var resp = await fetch('http://localhost:2000/auth/register',{
						method:'POST',
						body:fd
					});
					var data = await resp.json();

					console.log(data);
					setName("");
				 }
				}}/>
		
		
		</div>
		<div className="col-md-4 login-right-info">
			<h3 className="subhead-agileits">Why Join Elite Match</h3>
			<p className="para-agileits-w3layouts" >Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<ul>
				<li><i className="fa fa-check" aria-hidden="true"></i>Lorem ipsum</li>
				<li><i className="fa fa-check" aria-hidden="true"></i>Vivamus lacus</li>
				<li><i className="fa fa-check" aria-hidden="true"></i>quisquam est</li>
				<li><i className="fa fa-check" aria-hidden="true"></i>Pellentesque</li>
			</ul>
			<h5>Already a member....<i className="fa fa-hand-o-down" aria-hidden="true"></i></h5>
			<div className="sim-button button12"><a href="login.html">Login</a></div>
		</div>
		<div className="col-md-4 login-right-info right-info-find">
			<h3 className="subhead-agileits">Easy steps to find your life partner</h3>
			<ul>
				<li><i className="fa fa-ellipsis-v" aria-hidden="true"></i>Vivamus lacus nisl, suscipit in vehicula sit amet, sollicitudin eget eros.</li>
				<li><i className="fa fa-ellipsis-v" aria-hidden="true"></i>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet.</li>
				<li><i className="fa fa-ellipsis-v" aria-hidden="true"></i>Vivamus lacus nisl, suscipit in vehicula sit amet, sollicitudin eget eros.</li>
			</ul>
		</div>
	</div>
</div>
</div>
       
    )
}

export default Register;