import { useEffect, useState } from "react";

function Profile(props)
{
   
  let[user,setUser]=useState({})

    async function getData()
    {
        var slug = props.match.params.slug;

        var fd = new FormData();
        fd.append('slug',slug)
        var resp = await fetch("http://localhost:2000/user/profile",{
            method:'POST',
            body:fd,
        });
        var data = await resp.json();
		console.log(data);
        setUser(data);


    
      
    }

    useEffect(()=>{
		
        getData()
    },[]);

    return(
        <div>


<div className="banner-w3ls inner-banner-agileits" id="home">
	<div className="container">
		
                 <div className="header-inner">
					  <h1 className="logo">
						<a href="index.html">Elite<span>Match</span></a></h1>
						<nav className="navbar navbar-default">
							<div className="navbar-header">
								<button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span className="sr-only">Toggle navigation</span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
								</button>
							</div>
							
							<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul className="nav navbar-nav navbar-right">
									<li><a href="index.html">Home</a></li>
									<li><a href="about.html">About</a></li>
									<li><a href="matches.html">Matches</a></li>
									<li className="dropdown menu__item">
										<a href="#" className="dropdown-toggle menu__link"  data-toggle="dropdown" data-hover="Pages" role="button" aria-haspopup="true" aria-expanded="false">Pages<span className="caret"></span></a>
										<ul className="dropdown-menu">
											<li><a href="codes.html">Short Codes</a></li>
											<li><a href="icons.html">Icons</a></li>
										</ul>
									</li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</div>
							<div className="clearfix"> </div>	
						</nav>
						<div className="header-right-w3ls">
							<a href="login.html">Log In</a>
							<a href="register.html">Register</a>
						</div>
						<div className="clearfix"></div>
				</div>
</div>			
</div>

<div className="w3l_agileits_breadcrumbs">
   <div className="container">
		<div className="w3l_agileits_breadcrumbs_inner">
			<ul>
				<li><a href="index.html">Home</a><span>&#187;</span></li>
									
				<li>Single</li>
				</ul>
		</div>
	</div>
</div>

<div className="matches elite-app">
	<div className="container">
	<div className="tittle-agileinfo">
		<h3>{user.name}</h3>
	</div>
	<div className="matches-main-agileinfo">
		<div className="col-md-6 profile1"> 
			
		</div>
		<div className="col-md-6 person-info-agileits-w3layouts">
			<ul>
				<li><span> Height</span>:{user.height} </li>
				<li><span>Caste</span>: convallis</li>
				<li><span>Religion</span>: Nam arcu</li>
				<li><span>Profession</span>: Fashion Designer</li>
				<li><span>Marital Status</span>: Single</li>
				<li><span>Profile Created By</span>: Self</li>
				<li><span>Location</span>: Jackson, 538, USA</li>
				<li><span>To View Her Photo</span> <a href="Register.html">Register Free</a></li>
			</ul>
		</div>
		<div className="clearfix"> </div>
		<div className="about-person">
			<h6>About Myself:</h6>
			<p className="para-agileits-w3layouts"><span>Hi!, Iam Jessica </span>Nam arcu mauris, tincidunt sed convallis non, egestas ut lacus. Cras sapien urna, malesuada ut varius consequat, hendrerit nisl. Aliquam vestibulum, odio non ullamcorper malesuada.Nam arcu mauris, tincidunt sed convallis non, egestas ut lacus. Cras sapien urna, malesuada ut varius consequat, hendrerit nisl.</p>
		</div>
	</div>
		<div className="col-md-6 person-info-agileits-w3layouts">
			<h3>Education, Basics & Lifestyle</h3>
			<ul>
				<li><span>Education</span>: Designing</li>
				<li><span>Profession</span>: Fashion Designer</li>
				<li><span>Mother Tongue</span>: English</li>
				<li><span>Blood Group</span>: B+</li>
				<li><span>Complexion</span>: Fair</li>
				<li><span>Weight</span>: 45</li>
				<li><span>Diet</span>: Non-Veg</li>
			</ul>
		</div>
		<div className="col-md-6 person-info-agileits-w3layouts">
			<h3>Family Details</h3>
			<ul>
				<li><span>Fathers Name</span>: Lorem Ipsum</li>
				<li><span>Fathers Occupation</span>: vestibulum</li>
				<li><span>Mothers Name</span>: convallis</li>
				<li><span>Mothers Occupation</span>: vestibulum</li>
				<li><span>No. Of Brothers</span>: 2</li>
				<li><span>No. Of Sisters</span>: Not Specified</li>
			</ul>
		</div>
		<div className="col-md-6 person-info-agileits-w3layouts">
			<h3>What She Is Looking For</h3>
			<ul>
				<li><span>Age</span>: 26-28</li>
				<li><span>Marital Status</span>: Single</li>
				<li><span>Complexion</span>: Fair</li>
				<li><span>height</span>: 5'7'' - 5'9''</li>
				<li><span>Caste</span>: Doesn't Matter</li>
				<li><span>Religion</span>: Lorem Ipsum</li>
				<li><span>Drink</span>: Never Drinks</li>
				<li><span>Diet</span>: Non-Veg</li>
			</ul>
		</div>
	</div>
</div>

<div className="w3l_footer">
		<div className="container">
				<div className="w3ls_footer_grid">
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Location:</h4>
							<p>Matrimonium Elite, Ipswich,<br/> Foxhall Road, UK</p>
					</div>
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Contact us:</h4>
							<p><span>Phone : </span>505-222-5432</p>
							<p><span>Email : </span><a href="mailto:info@example.com">info@example.com</a></p>
					</div>
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Opening hours:</h4>
							<p>Working days (8am-9pm)</p>
							<p>Sundays (9am-1pm)</p>
					</div>
					<div className="clearfix"> </div>
				</div>
			
				<div className="footer-middle-agileinfo">
					<div className="footer-button">
					<div className="button-top-w3layouts">
						<div className="logo-icons-w3ls">
							<i className="fa fa-heart heart1" aria-hidden="true"></i>
							<i className="fa fa-heart-o heart2" aria-hidden="true"></i>
						</div>
					</div>
						<div className="sim-button button12"><a href="contact.html">Contact Us</a></div>
						<h2>We’re Committed to Service Excellence.</h2>
					</div>
				</div>
			
		</div>
</div>

<div className="botttom-nav-agileits">
	<ul>
		<li><a href="index.html">Home</a></li>
		<li><a href="about.html">About</a></li>
		<li><a href="matches.html">Matches</a></li>
		<li><a href="contact.html">Contact</a></li>
	</ul>
</div>
<div className="footer-w3layouts">
				<div className="container">
				<div className="agile-copy">
					<p>© 2017 Elite Match. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
				<div className="agileits-social">
					<ul>
							<li><a href="#"><i className="fa fa-facebook"></i></a></li>
							<li><a href="#"><i className="fa fa-twitter"></i></a></li>
							<li><a href="#"><i className="fa fa-rss"></i></a></li>
							<li><a href="#"><i className="fa fa-vk"></i></a></li>
						</ul>
				</div>
					<div className="clearfix"></div>
				</div>
			</div>


        </div>
    )
}

export default Profile;