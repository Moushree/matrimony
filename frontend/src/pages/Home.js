import MetaTags from 'react-meta-tags';
function Home()
{
    return(
        <div>
         
<div className="banner-w3ls" id="home">

		<MetaTags>
            <title>Matrimonium</title>
            <meta id="meta-description" name="description" content="Some description." />
            <meta id="og-title" property="og:title" content="Matrimonium" />
         
        </MetaTags>
	<div className="container">
	
                 <div className="header-inner">
					  <h1 className="logo">
						<a href="index.html">Elite<span>Match</span></a></h1>
						<nav className="navbar navbar-default">
							<div className="navbar-header">
								<button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span className="sr-only">Toggle navigation</span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
								</button>
							</div>
						
							<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul className="nav navbar-nav navbar-right">
									<li><a href="index.html" className="active">Home</a></li>
									<li><a href="about.html">About</a></li>
									<li><a href="matches.html">Matches</a></li>
									<li className="dropdown menu__item">
										<a href="#" className="dropdown-toggle menu__link"  data-toggle="dropdown" data-hover="Pages" role="button" aria-haspopup="true" aria-expanded="false">Pages<span className="caret"></span></a>
										<ul className="dropdown-menu">
											<li><a href="codes.html">Short Codes</a></li>
											<li><a href="icons.html">Icons</a></li>
										</ul>
									</li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</div>
							<div className="clearfix"> </div>	
						</nav>
						<div className="header-right-w3ls">
							<a href="login.html">Log In</a>
							<a href="register.html">Register</a>
						</div>
						<div className="clearfix"></div>
				</div>
			
			<div className="w3l_banner_info" >
				<div className="slider">
					<div className="callbacks_container">
								<ul className="rslides" id="slider3">
									<li>
										<div className="w3ls-info">
											 <h4>Be fabulous. Be empowered. Be Blown Away!</h4>
											<p>We’re Committed to Service Excellence By Elite Match.</p>
										</div>

									</li>
									<li>
										  <div className="w3ls-info">
											<h4>Dream it. Believe it. make it happen! </h4>
											<p>Checkout our list of best services and plan to best Finding.</p>
										</div>

									</li>
									<li>
										<div className="w3ls-info">
											<h4>Be fabulous. Be empowered. Be Blown Away!</h4>
											<p>Welcome to our Elite match - keep in touch with us...</p>
										</div>

									</li>

								</ul>
					</div>
				</div>
			</div>
			<div className="clearfix"></div>
	
				<div className="sim-button button12"><a href="about.html">More About Us</a></div>
			<div className="agileits-social">
					<ul>
							<li><a href="#"><i className="fa fa-facebook"></i></a></li>
							<li><a href="#"><i className="fa fa-twitter"></i></a></li>
							<li><a href="#"><i className="fa fa-rss"></i></a></li>
							<li><a href="#"><i className="fa fa-vk"></i></a></li>
						</ul>
				</div>
        </div>
</div>

	<div className="services" id="services">
		<div className="container">
		<div className="tittle-agileinfo">
				<h3>Assisted Services</h3>
			</div>
			<div className="col-md-6 w3_agileits_services_grids">
				<div className="w3_agileits_services_grid">
					<div className="w3_agileits_services_grid_agile">
						<div className="w3_agileits_services_grid_1">
							<i className="fa fa-search" aria-hidden="true"></i>
						</div>
						<h3>Search within reach</h3>
					</div>
				</div>
				<div className="w3_agileits_services_grid">
					<div className="w3_agileits_services_grid_agile">
						<div className="w3_agileits_services_grid_1">
							<i className="fa fa-list-alt" aria-hidden="true"></i>
						</div>
						<h3>Shortlisted matches</h3>
					</div>
				</div>
				<div className="w3_agileits_services_grid">
					<div className="w3_agileits_services_grid_agile">
						<div className="w3_agileits_services_grid_1">
							<i className="fa fa-phone" aria-hidden="true"></i>
						</div>
						<h3>Initiate communication</h3>
					</div>
				</div>
				<div className="w3_agileits_services_grid">
					<div className="w3_agileits_services_grid_agile">
						<div className="w3_agileits_services_grid_1">
							<i className="fa fa-users" aria-hidden="true"></i>
						</div>
						<h3>Meet the prospects</h3>
					</div>
				</div>
				<div className="clearfix"> </div>
			</div>
			<div className="col-md-6 regstr-r-w3-agileits">
			<div className="form-bg-w3ls">
				<h3 className="subhead-agileits white-w3ls">Interested in assisted services....?</h3>
				<p className="para-agileits-w3layouts">Leave your details below, well call you back.</p>
				<form action="#" method="post">
					<input type="text" name="Name" placeholder="Full name" required=""/>
					<input type="text" name="number" placeholder="Number" required=""/>
					<input type="email" name="Email" placeholder="Email" required=""/>
					<select className="form-control">
						<option>Select Location</option>
						<option>Bangalore</option>
						<option>Chandigarh</option>
						<option>Kolkata</option>
						<option>Ludhiana</option>
						<option>Mumbai</option>
						<option>Mysore</option>
						<option>Pune</option>
					</select>
					<input type="submit" value="Submit" className="button-w3layouts hvr-rectangle-out"/>
				</form>	
			</div>
			</div>
		</div>
	</div>

<div className="about-w3layouts" id="about">
	<div className="tittle-agileinfo">
				<h3>About Us</h3>
			</div>
	<div className="about-left-agileits">
		<div className="button">
			<a href="#small-dialog" className="play-icon popup-with-zoom-anim"><span className="glyphicon glyphicon-play-circle" aria-hidden="true"></span></a>
		</div>
		<div id="small-dialog" className="mfp-hide w3ls_small_dialog wthree_pop">		
			<div className="agileits_modal_body">
				<iframe src="https://player.vimeo.com/video/198296034"></iframe>
			</div>
		</div>
	</div>
	<div className="about-right">
		<h3 className="subheading-agileits-w3layouts"><span>Elite </span>Find Success Stories</h3>
		<p className="para-agileits-w3layouts">Duis sit amet nisi quis leo fermentum vestibulum vitae eget augue. Nulla quam nunc, vulputate id urna at, tempor tincidunt metus. Sed feugiat quam nec mauris mattis malesuada.</p>
		<div className="sim-button button12"><a href="matches.html">View Profiles</a></div>
	</div>
	<div className="clearfix"> </div>
</div>

 <div className="search-wthree">
 <div className="container">
	<h3 className="subheading-agileits-w3layouts">Start your journey with confidence.</h3>
	<div className="sim-button button12"><a href="matches.html">Search Now</a></div>
	<div className="clearfix"> </div>
 </div>
  </div>

<div className="middle-section-agileits-w3layouts">
<div className="container">
		<h3 className="subheading-agileits-w3layouts"><span>Elite </span>Match</h3>
		<p className="para-w3layouts">Exclusive Matchmaking Service for the Elite</p>
		<div className="sim-button button12"><a href="contact.html">Contact Us</a></div>
</div>	
</div>

	<div className="elite-app" >
			<div className="container">
	                   <div className="app-inner agile-w3l">
					     <div className="col-md-5 app-info">
						     <h4>Elite Match Apps</h4>
							 <p className="para-agileits-w3layouts">Nam arcu mauris, tincidunt sed convallis non, egestas ut lacus. Cras sapien urna, malesuada ut varius consequat, hendrerit nisl. Aliquam vestibulum, odio non ullamcorper malesuada.</p>
							 <div className="app-devices">
								<a href="#"><img src="images/app.png" alt=""/></a>
								<a href="#"><img src="images/app1.png" alt=""/></a>
								<div className="clearfix"> </div>
							</div>
							<p className="para-agileits-w3layouts"><a href="#">Click here </a>to know more about apps.</p>
						 </div>
						 <div className="col-md-7 app-img">
						    <img src="images/screens1.png" alt=" " className="img-responsive"/>
						 </div>
						 <div className="clearfix"></div>
					   </div>
			</div>
		</div>

	<div className="test" id="clients">
	<div className="container">
	<div className="tittle-agileinfo">
		<h3 className="white-w3ls">Happy Couples</h3>
	</div>
			<div className="test-gri1">
			<div id="owl-demo2" className="owl-carousel">
				<div className="test-grid1">
					<img src="images/t1.jpg" alt="" className="img-r"/>
					<h4>Abelard & Heloise</h4>
					<span>Couple 1</span>
					<p>Lorem ipsum dolor sit amet, Ut enim ad minim veniam, quis.Lorem ipsum dolor .</p>
				</div>	
				<div className="agile">
					<div className="test-grid1">
						<img src="images/t2.jpg" alt="" className="img-r"/>
						<h4>Bonnie & Clyde</h4>
						<span>Couple 2</span>
						<p>Lorem ipsum dolor sit amet, Ut enim ad minim veniam, quis.Lorem ipsum dolor .</p>
					</div>	
				</div>
				<div className="agile">
					<div className="test-grid1">
						<img src="images/t3.jpg" alt="" className="img-r"/>
						<h4>Jack & Sally</h4>
						<span>Couple 3</span>
						<p>Lorem ipsum dolor sit amet, Ut enim ad minim veniam, quis.Lorem ipsum dolor .</p>
					</div>	
				</div>					
			</div>
		</div>	

		</div>
	</div>

	<div className="gallery" id="gallery">
		<div className="tittle-agileinfo">
			<h3>Together by Elite Match</h3>
		</div>
			<ul id="flexiselDemo1">	
			<li>
				<div className="wthree_gallery_grid">
					<a href="images/g1.jpg" className="lsb-preview" data-lsb-group="header">
						<div className="view second-effect">
							<img src="images/g1.jpg" alt="" className="img-responsive" />
							<div className="mask">
								<p>Bonnie <i className="fa fa-heart-o" aria-hidden="true"></i> Clyde</p>
							</div>
						</div>	
					</a>
				</div>
			</li>
			<li>
				<div className="wthree_gallery_grid">
					<a href="images/g2.jpg" className="lsb-preview" data-lsb-group="header">
						<div className="view second-effect">
							<img src="images/g2.jpg" alt="" className="img-responsive" />
							<div className="mask">
								<p>Abelard <i className="fa fa-heart-o" aria-hidden="true"></i> Heloise</p>
							</div>
						</div>	
					</a>
				</div>
			</li>
			<li>
				<div className="wthree_gallery_grid">
					<a href="images/g3.jpg" className="lsb-preview" data-lsb-group="header">
						<div className="view second-effect">
							<img src="images/g3.jpg" alt="" className="img-responsive" />
							<div className="mask">
								<p>Henry <i className="fa fa-heart-o" aria-hidden="true"></i> Clare</p>
							</div>
						</div>	
					</a>
				</div>
			</li>
			<li>
				<div className="wthree_gallery_grid">
					<a href="images/g4.jpg" className="lsb-preview" data-lsb-group="header">
						<div className="view second-effect">
							<img src="images/g4.jpg" alt="" className="img-responsive" />
							<div className="mask">
								<p>Jack <i className="fa fa-heart-o" aria-hidden="true"></i> Sally</p>
							</div>
						</div>	
					</a>
				</div>
			</li>
			<li>
				<div className="wthree_gallery_grid">
					<a href="images/g5.jpg" className="lsb-preview" data-lsb-group="header">
						<div className="view second-effect">
							<img src="images/g5.jpg" alt="" className="img-responsive" />
							<div className="mask">
								<p>Henry <i className="fa fa-heart-o" aria-hidden="true"></i> Clare</p>
							</div>
						</div>	
					</a>
				</div>
			</li>
		</ul>


	</div>


<div className="w3l_footer">
		<div className="container">
				<div className="w3ls_footer_grid">
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Location:</h4>
							<p>Matrimonium Elite, Ipswich,<br/> Foxhall Road, UK</p>
					</div>
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Contact us:</h4>
							<p><span>Phone : </span>505-222-5432</p>
							<p><span>Email : </span><a href="mailto:info@example.com">info@example.com</a></p>
					</div>
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Opening hours:</h4>
							<p>Working days (8am-9pm)</p>
							<p>Sundays (9am-1pm)</p>
					</div>
					<div className="clearfix"> </div>
				</div>

				<div className="footer-middle-agileinfo">
					<div className="footer-button">
					<div className="button-top-w3layouts">
						<div className="logo-icons-w3ls">
							<i className="fa fa-heart heart1" aria-hidden="true"></i>
							<i className="fa fa-heart-o heart2" aria-hidden="true"></i>
						</div>
					</div>
						<div className="sim-button button12"><a href="contact.html">Contact Us</a></div>
						<h2>We’re Committed to Service Excellence.</h2>
					</div>
				</div>

		</div>
</div>

<div className="botttom-nav-agileits">
	<ul>
		<li><a href="index.html">Home</a></li>
		<li><a href="about.html">About</a></li>
		<li><a href="matches.html">Matches</a></li>
		<li><a href="contact.html">Contact</a></li>
	</ul>
</div>
<div className="footer-w3layouts">
				<div className="container">
				<div className="agile-copy">
					<p>© 2017 Elite Match. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
				<div className="agileits-social">
					<ul>
							<li><a href="#"><i className="fa fa-facebook"></i></a></li>
							<li><a href="#"><i className="fa fa-twitter"></i></a></li>
							<li><a href="#"><i className="fa fa-rss"></i></a></li>
							<li><a href="#"><i className="fa fa-vk"></i></a></li>
						</ul>
				</div>
					<div className="clearfix"></div>
				</div>
			</div>
</div> 

    )
}
export default Home;