import {useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
function Listuser()
{
	let [users, setUsers]= useState([]);
	let [religion, setReligion]= useState("");
	let [heightTo , setHeightTo]= useState("");
	let [heightFrom, setHightFrom]=useState("");

	async function getData()
	{

		var gendar = localStorage.getItem("gendar");
	
		var fd = new FormData();
		fd.append('gendar',gendar);

		var resp = await fetch('http://localhost:2000/user/list',{
			method:'POST',
			body:fd
		});
		var data = await resp.json();
		console.log(data);
		setUsers(data);
	}


	useEffect(()=>{
		getData();
	},[]);

    return(
        <div>

        
<div className="banner-w3ls inner-banner-agileits" id="home">
	<div className="container">
	
                 <div className="header-inner">
					  <h1 className="logo">
						<Link to="index.html">Elite<span>Match</span></Link></h1>
						<nav className="navbar navbar-default">
							<div className="navbar-header">
								<button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span className="sr-only">Toggle navigation</span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
								</button>
							</div>
						
							<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul className="nav navbar-nav navbar-right">
									<li><Link to="index.html">Home</Link></li>
									<li><Link to="about.html">About</Link></li>
									<li><Link to="matches.html">Matches</Link></li>
									<li className="dropdown menu__item">
										<Link to="#" className="dropdown-toggle menu__link"  data-toggle="dropdown" data-hover="Pages" role="button" aria-haspopup="true" aria-expanded="false">Pages<span className="caret"></span></Link>
										<ul className="dropdown-menu">
											<li><Link to="codes.html">Short Codes</Link></li>
											<li><Link to="icons.html">Icons</Link></li>
										</ul>
									</li>
									<li><Link to="contact.html">Contact</Link></li>
								</ul>
							</div>
							<div className="clearfix"> </div>	
						</nav>
						<div className="header-right-w3ls">
							<Link to="login.html">Log In</Link>
							<Link to="register.html">Register</Link>
						</div>
						<div className="clearfix"></div>
				</div>
</div>			
</div>

<div className="w3l_agileits_breadcrumbs">
   <div className="container">
		<div className="w3l_agileits_breadcrumbs_inner">
			<ul>
				<li><Link to="index.html">Home</Link><span>&#187;</span></li>
									
				<li>Register</li>
				</ul>
		</div>
	</div>
</div>

<div className="login elite-app">
	<div className="container">
	<div className="tittle-agileinfo">
				<h3>Find Your Matches</h3>
				
				<div className="row">
				<div className="col-md-4">
				<div className="w3_form_body_grid">
				
					<span>religion</span>
					<select className="frm-field required" name="religion"  onChange={(ev)=>{
						setReligion(ev.target.value);
					}}>
						<option value="null">--Select Religion--</option> 
						<option value="Hindu">Hindu</option>  
						<option value="Muslim">Muslim</option>   
						<option value="Christian">Christian</option>   
						<option value="Sikh">Sikh</option>   
						<option value="Jain">Jain</option>   
						<option value="Buddhist">Buddhist</option>
						<option value="">No Religious Belief</option>   						
					</select>

				</div>
				</div>
				
				<div className="col-md-4">
				<div className="w3_form_body_grid">
				
					<span>Height From</span>
					<input type="number" name="name"  required="" className="heighr-box" onChange={(ev)=>{
						setHightFrom(ev.target.value);
}}/>

				</div>
				</div>
				<div className="col-md-4">
				<div className="w3_form_body_grid">
				
					<span>Height To</span>
					<input type="number" name="name"  required="" className="heighr-box" onChange={(ev)=>{
						setHeightTo(ev.target.value);
					}}/>

				</div>
				</div>
				</div>
				<div className="row">
					<input type="button" value="search" className="btn btn-primary" onClick={async ()=>{
						var fd = new FormData();
						fd.append('religion',religion);
						fd.append('heightTo',heightTo);
						fd.append('heightFrom',heightFrom);

						var resp = await fetch("http://localhost:2000/user/search/",{
							body:fd,
							method:'POST',
						});

						var data = await resp.json();
						// console.log(data);
						setUsers(data)
					}	
					}/>
				</div>
			</div>
	
			<div className="row">
			{users.map((u)=>
                <div className="col-md-3" key={u._id}>
                <div className="card" >
                        <img className="card-img-top" src={"http://localhost:2000/profileimg/"+u.profile} alt="Card image"/>
                        <div className="card-body">
                        <h4 className="card-title"><strong>Name:</strong> {u.name}</h4>
                        <p className="card-text"><strong>Religion:</strong> {u.religion}</p>
						<p className="card-text"><strong>Height:</strong> {u.height}</p>
						<div className="card-button">
							<Link to={"/profile/"+u.slug} className="btn btn-primary">See Profile</Link>
						</div>
                        
                        </div>
                    </div>
                </div>
			)}
            </div>

	</div>
</div>
</div>
       
    )
}

export default Listuser;