import {useState} from 'react';
function Register()
{

	let [email,setEmail]=useState('');
	let [password,setPassword]=useState('');
	let [lerr,setLerr]=useState(false);

    return(
        <div>


<div className="banner-w3ls inner-banner-agileits" id="home">
	<div className="container">
		
                 <div className="header-inner">
					  <h1 className="logo">
						<a href="index.html">Elite<span>Match</span></a></h1>
						<nav className="navbar navbar-default">
							<div className="navbar-header">
								<button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span className="sr-only">Toggle navigation</span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
									<span className="icon-bar"></span>
								</button>
							</div>
						
							<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul className="nav navbar-nav navbar-right">
									<li><a href="index.html">Home</a></li>
									<li><a href="about.html">About</a></li>
									<li><a href="matches.html">Matches</a></li>
									<li className="dropdown menu__item">
										<a href="#" className="dropdown-toggle menu__link"  data-toggle="dropdown" data-hover="Pages" role="button" aria-haspopup="true" aria-expanded="false">Pages<span className="caret"></span></a>
										<ul className="dropdown-menu">
											<li><a href="codes.html">Short Codes</a></li>
											<li><a href="icons.html">Icons</a></li>
										</ul>
									</li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</div>
							<div className="clearfix"> </div>	
						</nav>
						<div className="header-right-w3ls">
							<a href="login.html">Log In</a>
							<a href="register.html">Register</a>
						</div>
						<div className="clearfix"></div>
				</div>
</div>			
</div>

<div className="w3l_agileits_breadcrumbs">
   <div className="container">
		<div className="w3l_agileits_breadcrumbs_inner">
			<ul>
				<li><a href="index.html">Home</a><span>&#187;</span></li>
									
				<li>Login</li>
				</ul>
		</div>
	</div>
</div>

<div className="login elite-app">
	<div className="container">
	<div className="tittle-agileinfo">
				<h3>Login Now</h3>
			</div>
	<div className="col-md-8 login-form-w3-agile">
			
				<div classNameName="w3_form_body_grid">
					<span>Email</span>
					<input type="email" name="Email" placeholder="Email" required="" onChange={(ev)=>{
						setEmail(ev.target.value)
					}}/>
				</div>
				<div classNameName="w3_form_body_grid w3_form_body_grid1">
					<span>Password</span>
					<input type="password" name="Password" placeholder="Password" required="" onChange={(ev)=>{
						setPassword(ev.target.value)
					}}/>
				</div>
				<div className="agile_remember">
					<div className="agile_remember_left">
						<div className="check">
							<label className="checkbox"><input type="checkbox" name="checkbox"/><i> </i>remember me</label>
						</div>
					</div>
					<div className="agile_remember_right">
						<a href="#">Forgot Password?</a>
					</div>
					<div className="clearfix"> </div>
				</div>
				<input type="button" value="Sign In" onClick={ async (ev)=>{
					var fd = new FormData()
					fd.append('email',email);
					fd.append('password',password);
					

					var resp = await fetch('http://localhost:2000/auth/login',{
						method:'POST',
						body:fd
					});
					var data = await resp.json();

					console.log(data);
					if(data.msg=="invalid login"){
                    setLerr(true)
					}else{
						
						setLerr(false);
						localStorage.setItem("uid",data._id);
						localStorage.setItem("uname",data.name);
						localStorage.setItem("gendar",data.gandar);
						//console.log(data);
						window.location = "/listusers";
					}
				}}/>
		
			
			<h4>Continue With</h4>
			
		</div>
		<div className="col-md-4 login-right-info">
			<h3 className="subhead-agileits">Why Join Elite Match</h3>
			<p className="para-agileits-w3layouts" >Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<ul>
				<li><i className="fa fa-check" aria-hidden="true"></i>Lorem ipsum</li>
				<li><i className="fa fa-check" aria-hidden="true"></i>Vivamus lacus</li>
				<li><i className="fa fa-check" aria-hidden="true"></i>quisquam est</li>
				<li><i className="fa fa-check" aria-hidden="true"></i>Pellentesque</li>
			</ul>
			<h5>Don't have an account?<i className="fa fa-hand-o-down" aria-hidden="true"></i></h5>
			<div className="sim-button button12"><a href="register.html">Register</a></div>
		</div>
	</div>
</div>

<div className="w3l_footer">
		<div className="container">
				<div className="w3ls_footer_grid">
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Location:</h4>
							<p>Matrimonium Elite, Ipswich,<br/> Foxhall Road, UK</p>
					</div>
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Contact us:</h4>
							<p><span>Phone : </span>505-222-5432</p>
							<p><span>Email : </span><a href="mailto:info@example.com">info@example.com</a></p>
					</div>
					<div className="col-md-4 w3ls_footer_grid_left">
							<h4>Opening hours:</h4>
							<p>Working days (8am-9pm)</p>
							<p>Sundays (9am-1pm)</p>
					</div>
					<div className="clearfix"> </div>
				</div>
				
				<div className="footer-middle-agileinfo">
					<div className="footer-button">
					<div className="button-top-w3layouts">
						<div className="logo-icons-w3ls">
							<i className="fa fa-heart heart1" aria-hidden="true"></i>
							<i className="fa fa-heart-o heart2" aria-hidden="true"></i>
						</div>
					</div>
						<div className="sim-button button12"><a href="contact.html">Contact Us</a></div>
						<h2>We’re Committed to Service Excellence.</h2>
					</div>
				</div>
				
		</div>
</div>

<div className="botttom-nav-agileits">
	<ul>
		<li><a href="index.html">Home</a></li>
		<li><a href="about.html">About</a></li>
		<li><a href="matches.html">Matches</a></li>
		<li><a href="contact.html">Contact</a></li>
	</ul>
</div>
<div className="footer-w3layouts">
				<div className="container">
				<div className="agile-copy">
					<p>© 2017 Elite Match. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
				<div className="agileits-social">
					<ul>
							<li><a href="#"><i className="fa fa-facebook"></i></a></li>
							<li><a href="#"><i className="fa fa-twitter"></i></a></li>
							<li><a href="#"><i className="fa fa-rss"></i></a></li>
							<li><a href="#"><i className="fa fa-vk"></i></a></li>
						</ul>
				</div>
					<div className="clearfix"></div>
				</div>
			</div>



        </div>
       
    )
}

export default Register;