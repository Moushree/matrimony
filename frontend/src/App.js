import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Switch,Link, Route} from 'react-router-dom';
import Home from './pages/Home';
import Register from './pages/Register';
import Listuser from './pages/Listuser';
import Signin from './pages/Signin';
import Profile from './pages/Profile';
function App() {
  return (
    
    <div>
    <BrowserRouter>
     <Switch>
       <Route exact path="/" component={Home}/>
       <Route exact path="/register" component={Register}/>
       <Route exact path="/listusers" component={Listuser}/>
       <Route exact path="/login" component={Signin}/>
       <Route exact path="/profile/:slug" component={Profile}/>
     </Switch>
    </BrowserRouter>
      
     
      
    </div>
  );
}

export default App;
